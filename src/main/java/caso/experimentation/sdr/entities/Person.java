package caso.experimentation.sdr.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Person {
    @Id()
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    @RestResource(path = "adresses")
    @JoinTable(name = "person_adresses", joinColumns =@JoinColumn(name = "person_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "adresses_id", referencedColumnName = "id"))
    @OneToMany(cascade = CascadeType.PERSIST)
    Collection<Adress> myAdresses = new ArrayList<>();
}
