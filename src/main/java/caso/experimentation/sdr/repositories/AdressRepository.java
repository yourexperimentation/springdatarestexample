package caso.experimentation.sdr.repositories;

import caso.experimentation.sdr.entities.Adress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AdressRepository extends CrudRepository<Adress, Long>{

}
