package caso.experimentation.sdr.repositories;

import caso.experimentation.sdr.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource
public interface PersonRepository extends JpaRepository<Person, Long> {

    @RestResource(path = "byLastName", rel = "byLastName" )
    List<Person> findByLastName(@Param("name") String name);

}
